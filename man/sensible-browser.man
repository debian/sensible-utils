.\" -*- nroff -*-
.TH SENSIBLE-BROWSER 1 "12 Jan 2020" "Debian"
.SH NAME
sensible-browser \- sensible web browsing
.SH SYNOPSIS
.BR sensible-browser " url"
.SH DESCRIPTION
.B sensible-browser
makes sensible decisions on which web browser to call.
Programs in Debian can use this script
as their default web browser or emulate their behavior.
.br
.B BROWSER
environment variable could be set, and will be used if set.
Any string acceptable as a command_string operand to the 
.B sh \-c
command shall be valid (this is the same behavior of the
.B EDITOR
variable as documented in
.BR \%environ (7)).
.SH "SEE ALSO"
.BR environ (7)
.SH "STANDARD"
Documentation of behavior of sensible-utils under a debian system is available under
section 11.4 of debian-policy usually installed under
/usr/share/doc/debian-policy (you might need to install debian-policy)
